<?php
 
class User_model extends CI_Model{
	
    function __construct(){
		
        parent::__construct();
		$this->load->database();
		$this->load->model(array('Email_model','Openstack_model'));
		
    }
    
    function cancelVM($identifier_vm){
		
		$this->db->set('status', 5);
		$this->db->where('identifier_vm', $identifier_vm);
		$this->db->update('list_virtual_machines');
		
    }
    
    function deleteVM($identifier_vm){
		
		$id_vm = $this->db->get_where('list_virtual_machines',array('identifier_vm'=>$identifier_vm))->row()->id_vm;
		
		$this->Openstack_model->deleteVM($id_vm);
		
		$this->db->set('status', 4);
		$this->db->where('identifier_vm', $identifier_vm);
		$this->db->update('list_virtual_machines');
		
    }
	
    function rebootVM($identifier_vm){
		
		$id_vm = $this->db->get_where('list_virtual_machines',array('identifier_vm'=>$identifier_vm))->row()->id_vm;
		$this->Openstack_model->rebootVM($id_vm);		
		
    }
	
    function getConsoleVM($id_vm){
		
		return $this->Openstack_model->getConsoleVM($id_vm);		
		
    }
	
    function getRequestedVM($id){
		
		$this->db->from('list_virtual_machines');
		$this->db->where('id_user', $id);
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
    }
	
    function getVM($id){
		
		$this->db->from('list_virtual_machines');
		$this->db->where('id_user', $id);
		//$this->db->where('status', 2);
		$this->db->order_by("id", "desc");
		return $this->db->get()->result();		
    }
	
    function getSum($id){
		
		$this->db->from('list_virtual_machines');
		$this->db->where('id_user', $id);
		$this->db->where('status', '1');
		$vm_request = $this->db->count_all_results();
		
		$this->db->from('list_virtual_machines');
		$this->db->where('id_user', $id);
		$this->db->where('status', '2');
		$vm_running = $this->db->count_all_results();
		
		$sum = 0;
		
		if(($vm_request != 0) || ($vm_running != 0)){
			$sum = 1;
		}
		
		return $sum;
    }
	
	function requestVM($params){
		$full_params = $params;
		unset($params["image_name"]);

		$this->db->insert('list_virtual_machines',$params);
		
		$this->load->library('ion_auth');
		
		$this->Email_model->requestEmail($full_params, $this->ion_auth->user()->row()->email, $this->ion_auth->user()->row()->name);
		//$this->Email_model->requestEmailAdmin($params, $this->ion_auth->user()->row()->email, $this->ion_auth->user()->row()->name);
		
        return $this->db->insert_id();
		
	}
	 
}
