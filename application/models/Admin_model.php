<?php
 
class Admin_model extends CI_Model{
	
    function __construct(){
        parent::__construct();
		$this->load->database();
		$this->load->model(array('Openstack_model','Email_model'));
    }
    
    function getVM($identifier_vm){
        return $this->db->get_where('list_virtual_machines',array('identifier_vm'=>$identifier_vm))->row();
    }
	
	function updateAcceptedVMID($identifier_vm,$vm_id){
		$this->db->set('id_vm', $vm_id);
		$this->db->set('status', 2);
		$this->db->where('identifier_vm', $identifier_vm);
		$this->db->update('list_virtual_machines');
	}
	
	function updateFloatingIP($identifier_vm,$ip){
		$this->db->set('ip_vm', $ip);
		$this->db->where('identifier_vm', $identifier_vm);
		$this->db->update('list_virtual_machines');
	}
	
	function updateDeclinedVM($identifier_vm){
		$this->db->set('status', 3);
		$this->db->where('identifier_vm', $identifier_vm);
		$this->db->update('list_virtual_machines');
	}
	
	function updateDeletedVM($identifier_vm){
		$this->db->set('status', 4);
		$this->db->where('identifier_vm', $identifier_vm);
		$this->db->update('list_virtual_machines');
	}
	
	function assignAdminRole($user_id){
		$data = array(
				'user_id' => $user_id,
				'group_id' => 2,
		);

		$this->db->insert('users_groups', $data);
		
		$this->db->set('is_admin', 1);
		$this->db->where('id', $user_id);
		$this->db->update('users');
	}
	
	function deleteAdminRole($user_id){
		$this->db->delete('users_groups', array('user_id' => $user_id,'group_id' => 2));
		
		$this->db->set('is_admin', 0);
		$this->db->where('id', $user_id);
		$this->db->update('users');
	}
	
	function getEmailbyID($user_id){
        return $this->db->get_where('users',array('id'=>$user_id))->row()->email;
	}
	
	function getUserbyIdentifier($identifier_vm){
        return $this->db->get_where('list_virtual_machines',array('identifier_vm'=>$identifier_vm))->row()->id_user;
	}
	
	function getParamSatu($id_user){
        return $this->db->get_where('users',array('id'=>$id_user))->row()->user_id_os;
	}
	
	function getParamDua($id_user){
        return $this->db->get_where('users',array('id'=>$id_user))->row()->param2;
	}
	
	function createVM($identifier_vm){
		
		//ambil detail vm
		$detail = $this->getVM($identifier_vm);
		
		$param1_os = $this->getParamSatu($detail->id_user);
		$param2_os = $this->getParamDua($detail->id_user);
		
		//buat vm
		$params['password'] = $this->Openstack_model->createVM($detail, $param1_os, $param2_os);
		
		//ngambil id vm
		$vm_id = $this->Openstack_model->getVMID($detail->identifier_vm);
		
		//simpen id vm
		$this->updateAcceptedVMID($identifier_vm,$vm_id);
		
		$email = $this->getUserbyIdentifier($identifier_vm);
		$email = $this->getEmailbyID($email);
		
		$detail = $this->getVM($identifier_vm);
		
		$name = $detail->user_name;
		$server_id = $detail->id_vm;
		
		sleep(5);
		
		$params['ip'] = $this->Openstack_model->assignFloatingIP($server_id);
		$params['hostname'] = $detail->hostname;
		
		$this->updateFloatingIP($identifier_vm,$params['ip']);
		
		$this->Email_model->approvedEmailAdmin($params, $email, $name);
		
		//print_r($params);
	}
	
	function listUser(){
		
		$this->db->from('users');
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
		
	}
	
	function listVM(){
		
		$this->db->from('list_virtual_machines');
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
		
	}
	
	function listRequestedVM(){
		
		$this->db->from('list_virtual_machines');
		$this->db->where('status', 1);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
		
	}
	
	function listRejectedVM(){
		
		$this->db->from('list_virtual_machines');
		$this->db->where('status', 3);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
        //return $this->db->get_where('list_virtual_machines',array('status'=>'3'))->result();
		
	}
	
	function deleteVM($identifier_vm){
		
		$id_vm = $this->db->get_where('list_virtual_machines',array('identifier_vm'=>$identifier_vm))->row()->id_vm;
		
		$this->Openstack_model->deleteVM($id_vm);
		$this->updateDeletedVM($identifier_vm);
	}
}