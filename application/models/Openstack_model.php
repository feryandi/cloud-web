<?php
 
class Openstack_model extends CI_Model{
	
    function __construct(){
        parent::__construct();
		$this->load->library('ion_auth');
		require 'vendor/autoload.php';
    }
	
	private $project = 'e4fd29c0f8e44efa91a1b64aa11d5e83';
	private $region = 'RegionOne';
	
	function authOpenStack(){
	
		$openstack = new OpenStack\OpenStack([
			'authUrl' => 'http://10.14.36.3:5000/v3',
			'region'  => $this->region,
			'user'    => [
				'id'       => '22b5ef0652a24d179a3acde1b1203298',
				'password' => 'ba01c561c5a74'
			],
			'scope'   => ['project' => ['id' => $this->project]]
		]);
		
		return $openstack;         
	}
	
	function authUser($param1, $param2){
	
		$openstack = new OpenStack\OpenStack([
			'authUrl' => 'http://10.14.36.3:5000/v3',
			'region'  => $this->region,
			'user'    => [
				'id'       => $param1,
				'password' => $param2
			],
			'scope'   => ['project' => ['id' => $this->project]]
		]);
		
		return $openstack;         
	}
	
	function createAccount($email,$param1,$param2){
		
		$openstack = $this->authOpenStack();
		
		$identity = $openstack->identityV3();

		$user = $identity->createUser([
			'defaultProjectId' => $this->project,
			'description'      => 'user_private_cloud_dds',
			'domainId'         => 'default',
			'email'            => $email,
			'enabled'          => true,
			'name'             => $param1,
			'password'         => $param2
		]);
		
		//print_r($user);
		
	}
	
	function grantRole($email,$id_user){
		
		$openstack = $this->authOpenStack();
		
		$identity = $openstack->identityV3();

		foreach ($identity->listUsers() as $user){
			
			if($user->email == $email){
				
 				$identity = $openstack->identityV3(['region' => $this->region]);
				$project = $identity->getProject($this->project);

				$project->grantUserRole([
					'userId' => $user->id,
					'roleId' => '9fe2ff9ee4384b1894a90878d3e92bab',
				]);
				
				$data = array(
								'user_id_os' => $user->id,
							 );
							 
				//$this->load->library('ion_auth');
				$this->ion_auth->update($id_user, $data);
			}
		}
	}
	
	function getActiveImages(){
		$openstack = $this->authOpenStack();
		
		$compute = $openstack->computeV2(['region' => $this->region]);

		$images = $compute->listImages(['status' => 'ACTIVE']);

		return  $images;
	}
	
	function getVMID($identifier){
		
		$openstack = $this->authOpenStack();
		
		$compute = $openstack->computeV2(['region' => $this->region]);
		$servers = $compute->listServers(true);
		
		foreach ($servers as $server) {
			if($server->metadata['identifier'] == $identifier){
				return($server->id);
			}
		}
	}
	
	function createVM($detail, $param1, $param2){
		
		$openstack = $this->authOpenStack();
		/*
		print_r($openstack);
		echo '<br/>';
		echo '<br/>';
		echo '<br/>';
		echo '<br/>';
		
		print_r($param1);
		echo '<br/>';
		print_r($param2); 
		
		echo '<br/>';
		echo '<br/>';
		echo '<br/>';
		echo '<br/>'; */
		
		//$openstack = $this->authUser($param1, $param2);
		
		//print_r($openstack);
		//exit; 
		
		$compute = $openstack->computeV2(['region' => $this->region]);

		$password = substr(md5(time()),0,6);
		
		$usrData = base64_encode('#cloud-config
ssh_pwauth: True
manage_etc_hosts: true
users:
  - name: telkom
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
chpasswd:
  list: |
    telkom:' . $password . '
  expire: True');
  
		//exit;
		$options = [

			'name'     => $detail->hostname,
			'imageId'  => $detail->image,
			'flavorId' => $detail->flavor,
			//'keyName'  => $detail->keypair,
			
			'networks'  => [
				['uuid' => 'a8bed5ef-d012-4530-8c54-c587d4437d01']
			],

			'metadata' => ['identifier' => $detail->identifier_vm],
			'userData' => $usrData,
		];

		$server = $compute->createServer($options);
		
		return $password;
		
	}
	
	function deleteVM($id_vm){
		
		$openstack = $this->authOpenStack();
		
		$compute = $openstack->computeV2(['region' => $this->region]);
		
		$server = $compute->getServer(['id' => $id_vm]);

		$server->delete();
	}
	
	function rebootVM($id_vm){

		$openstack = $this->authOpenStack();
		
		$compute = $openstack->computeV2(['region' => $this->region]);
		
		$server = $compute->getServer(['id' => $id_vm]);

		$server->reboot();
	}
	
	function getConsoleVM($id_vm){

		$openstack = $this->authOpenStack();
		
		$compute = $openstack->computeV2(['region' => $this->region]);
		
		$server = $compute->getServer(['id' => $id_vm]);

		$console = $server->getVncConsole();

		return $console['url'];
		
	}
	
	function getImages(){

		$openstack = $this->authOpenStack();
		
		$images = $openstack->imagesV2()->listImages();

		return $images;
	}
	
	function getFlavors(){
		
		$openstack = $this->authOpenStack();
		
		$compute = $openstack->computeV2(['region' => $this->region]);

		$flavors = $compute->listFlavors();

		return $flavors;
	}
	
	function getKeypairs($param1,$param2){
		
		$openstack = new OpenStack\OpenStack([
			'authUrl' => 'http://10.14.36.4:5000/v3',
			'region'  => $this->region,
			'user'    => [
				'id'       => $param1,
				'password' => $param2,
			],
			'scope'   => ['project' => ['id' => 'e4fd29c0f8e44efa91a1b64aa11d5e83']]
		]);
		
		$openstack = $this->authOpenStack();
		
		$compute = $openstack->computeV2(['region' => $this->region]);

		$keypairs = $compute->listKeypairs();

		return $keypairs;
	}
	
	function getFloatingIP(){
		
		$openstack = $this->authOpenStack();
		
		$floatingIps = $openstack->networkingV2ExtLayer3()
                         ->listFloatingIps();

		foreach ($floatingIps as $floatingIp) {
			if($floatingIp->status == 'DOWN'){
				$ip = $floatingIp->floatingIpAddress;
			}
		}
		
		return $ip;
		
	}
	
	function getToken(){
			
		$authData = '{
						"auth": {
							"identity": {
								"methods": [
									"password"
								],
								"password": {
									"user": {
										"id": "22b5ef0652a24d179a3acde1b1203298",
										"password": "ba01c561c5a74"
									}
								}
							},
							"scope": {
								"project": {
									"id": "e4fd29c0f8e44efa91a1b64aa11d5e83"
								}
							}
						}
					}';

		$ch = curl_init('http://10.14.36.3:5000/v3/auth/tokens/');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $authData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json'
		));
		
		$result = curl_exec($ch);
		curl_close($ch);
		
		//$result = json_decode($result, true);
		
		//print_r(substr($result, 90, 50));
		//echo "<br/>";
		$token = substr($result, 106, 34);
		//print_r($token);
		
		return $token;
		
	}
	
	
	
	function assignFloatingIP($server_id){
		
		$floatingIP = $this->getFloatingIP();
		$server_id = $server_id;
		$token = $this->getToken();
		
		$floatData = '{
						"addFloatingIp" : {
							"address": "' .  $floatingIP . '"
						}
					}';
		
		$ch = curl_init('http://10.14.36.3:8774/v2.1/e4fd29c0f8e44efa91a1b64aa11d5e83/servers/'. $server_id .'/action');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $floatData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'X-Auth-Token: ' . $token
		));
		
		$result = curl_exec($ch);
		curl_close($ch);
		
		//$result = json_decode($result, true);
		
		return $floatingIP;
	}
	
}
