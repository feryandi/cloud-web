<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		
        parent::__construct();
		$this->load->model(array('Admin_model','User_model','Auth_model'));
		$this->load->library('ion_auth');
		
		if(!$this->ion_auth->logged_in()){
			redirect('auth', 'refresh');
		}
		
		if(!$this->ion_auth->is_admin()){
			redirect('dashboard/index', 'refresh');
		}
		
    }
	
	public function index(){
		
		//buat login
		//$this->ion_auth->login('roosenorahman@gmail.com', '12345678', true);
		//exit; 
		$vms = $this->Admin_model->listVM();
		
		$images = $this->Openstack_model->getActiveImages();
		$imagename_dict = array();

		foreach ($images as $image) {
			$imagename_dict[$image->id] = $image->name;
		}

		for($i=0; $i<count($vms); $i++){
			if( isset($imagename_dict[$vms[$i]->image]) ) {
				$vms[$i]->image = $imagename_dict[$vms[$i]->image];
			} else {				
				$vms[$i]->image = "Unknown";
			}
		}
		
		$data['vms'] = $vms;
		$data['name'] = $this->ion_auth->user()->row()->name;
		$this->load->view('admin/manage-vm', $data); 
		
	}
	
	public function users(){

		$users = $this->Admin_model->listUser();
		
		$data['users'] = $users;
        $this->load->view('admin/list-user',$data);
		
	}
	
	public function assignAdminRole($id){
		
		$this->Admin_model->assignAdminRole($id);
        $this->load->view('admin/list-user',$data);
		
		
	}
	
	public function deleteAdminRole($id){
		
		$this->Admin_model->deleteAdminRole($id);
        $this->load->view('admin/list-user',$data);
		
	}
	
	public function requestedVM(){

		$vms = $this->Admin_model->listRequestedVM();
		
		$images = $this->Openstack_model->getActiveImages();
		$imagename_dict = array();

		foreach ($images as $image) {
			$imagename_dict[$image->id] = $image->name;
		}

		for($i=0; $i<count($vms); $i++){
			if( isset($imagename_dict[$vms[$i]->image]) ) {
				$vms[$i]->image = $imagename_dict[$vms[$i]->image];
			} else {				
				$vms[$i]->image = "Unknown";
			}
		}
		
		$data['vms'] = $vms;
		$data['name'] = $this->ion_auth->user()->row()->name;
        $this->load->view('admin/requested-vm',$data);
		
	}
	
	public function rejectedVM(){

		$vms = $this->Admin_model->listRejectedVM();
		
		$images = $this->Openstack_model->getActiveImages();
		$imagename_dict = array();

		foreach ($images as $image) {
			$imagename_dict[$image->id] = $image->name;
		}

		for($i=0; $i<count($vms); $i++){
			if( isset($imagename_dict[$vms[$i]->image]) ) {
				$vms[$i]->image = $imagename_dict[$vms[$i]->image];
			} else {				
				$vms[$i]->image = "Unknown";
			}
		}
		
		$data['vms'] = $vms;
		$data['name'] = $this->ion_auth->user()->row()->name;
        $this->load->view('admin/rejected-vm',$data);
		
	}
	
	public function listVM(){

		$vms = $this->Admin_model->listVM();
		
		$images = $this->Openstack_model->getActiveImages();
		$imagename_dict = array();

		foreach ($images as $image) {
			$imagename_dict[$image->id] = $image->name;
		}

		for($i=0; $i<count($vms); $i++){
			if( isset($imagename_dict[$vms[$i]->image]) ) {
				$vms[$i]->image = $imagename_dict[$vms[$i]->image];
			} else {				
				$vms[$i]->image = "Unknown";
			}
		}
		
		$data['vms'] = $vms;
        $this->load->view('admin/index',$data);
		
	}
	
	public function acceptRequest($identifier_vm){

		$this->Admin_model->createVM($identifier_vm);
		redirect('admin/index', 'refresh');
		
	}
	
	public function declineRequest($id_vm){
		
		$this->Admin_model->updateDeclinedVM($id_vm);
		redirect('admin/index', 'refresh');
		
	}
	
	public function deleteVM($identifier_vm){
		
		$this->Admin_model->deleteVM($identifier_vm);
		redirect('admin/index', 'refresh');
		
	}
	
	public function rebootVM($identifier_vm){
		
		$this->Admin_model->rebootVM($identifier_vm);
        redirect('admin/index','refresh');
		
	}
	
	public function getF(){
		$this->Openstack_model->assignFloatingIP();
	}
}
