<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		
        parent::__construct();
		
		$this->load->model(array('User_model','Openstack_model','Auth_model'));
		$this->load->library('ion_auth');
		
		if(!$this->ion_auth->logged_in()){
			redirect('auth', 'refresh');
		}
		
		/*if($this->ion_auth->is_admin()){
			redirect('auth', 'refresh');
		}*/
		
    }
	
/* 	private function temp(){
		
		$this->load->view('dashboard/detail-vm');
		
	}
 */	
	public function index(){
				 
		$id = $this->ion_auth->user()->row()->id;
		
		$vms = $this->User_model->getVM($id);
		
		$images = $this->Openstack_model->getActiveImages();
		$imagename_dict = array();

		foreach ($images as $image) {
			$imagename_dict[$image->id] = $image->name;
		}

		for($i=0; $i<count($vms); $i++){
			if( isset($imagename_dict[$vms[$i]->image]) ) {
				$vms[$i]->image = $imagename_dict[$vms[$i]->image];
			} else {				
				$vms[$i]->image = "Unknown";
			}
		}

		$data['vms'] = $vms;
		$data['name'] = $this->ion_auth->user()->row()->name;
		$data['sum'] = $this->User_model->getSum($id);
		
		$this->load->view('dashboard/manage-vm', $data); 
		 
	}
	
	public function request(){
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('hostname','Hostname','required|max_length[50]');
		$this->form_validation->set_rules('usage_purpose','Usage Purpose','required|max_length[300]');
		$this->form_validation->set_rules('supervisor_phone_number','Supervisor Phone Number','required');
		$this->form_validation->set_rules('supervisor','Supervisor','required|max_length[100]');
		
		if($this->form_validation->run()) 
        {
            $params = array(
				'identifier_vm' => md5(time() . $this->input->post('id_user')),
				'user_name' => $this->input->post('user_name'),
				'id_user' => $this->ion_auth->user()->row()->id,
				'id_vm' => 'default',
				'ip_vm' => 'default',
				'image' => $this->input->post('os-value'),
				'image_name' => $this->input->post('os-name'),
				'flavor' => $this->input->post('flavor-value'),
				'keypair' => 'not-set',
				'hostname' => $this->input->post('hostname'),
				'usage_purpose' => $this->input->post('usage_purpose'),
				'supervisor' => $this->input->post('supervisor'),
				'supervisor_phone_number' => $this->input->post('supervisor_phone_number'),
				'date_request' => date('H:i:s d-m-Y'),
				'status' => 1,
				'date_approval' => 'default',
				'id_admin' => '0',
            );
			
            $request = $this->User_model->requestVM($params);
            redirect('dashboard');
			
        }else{
			
			$images = $this->Openstack_model->getImages();
			$flavors = $this->Openstack_model->getFlavors();
			$keypairs = $this->Openstack_model->getKeypairs($this->ion_auth->user()->row()->param1, $this->ion_auth->user()->row()->param2);
			
 			$data['images'] = $images;
			$data['flavors'] = $flavors;
			$data['keypairs'] = $keypairs;
			$data['name'] = $this->ion_auth->user()->row()->name;
			
            $this->load->view('dashboard/create-vm', $data);
        }
	}
	
	public function requestedVM(){
		
		$id = $this->ion_auth->user()->row()->id;
		
		$vms = $this->User_model->getRequestedVM($id);
		
		for($i=0; $i<count($vms); $i++){
			$vms[$i]->image = $this->Openstack_model->getImageName($vms[$i]->image);
		}
		
		$data['vms'] = $vms;
		$data['name'] = $this->ion_auth->user()->row()->name;
		$this->load->view('dashboard/requested-vm', $data); 
		 
	}
	
	public function cancelVM($identifier_vm){
		
		$this->User_model->cancelVM($identifier_vm);
		redirect('dashboard/index','refresh');
	}
	
	public function deleteVM($identifier_vm){
		
		$this->User_model->deleteVM($identifier_vm);
        redirect('dashboard/index','refresh');
		
	}
	
	public function rebootVM($identifier_vm){
		
		$this->User_model->rebootVM($identifier_vm);
        	redirect('dashboard/index','refresh');
		
	}
	
	public function getConsoleVM($id_vm){
		
		$console = $this->User_model->getConsoleVM($id_vm);
		redirect($console, 'refresh');
		//exit;
		//$data['console'] = $console;
		//$this->load->view('dashboard/console', $data);
		
		//return $console;
		
	}
	
}
