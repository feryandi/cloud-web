<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <title></title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/semantic.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/semantic.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/custom-home.js') ?>"></script>
</head>

<body>