<?php $this->load->view('header'); ?>
<?php // echo validation_errors(); ?>

<?php // echo form_open('dashboard/create',array("class"=>"form-horizontal")); ?>

<!-- Page Contents -->
<div class="pusher">

<div class="ui teal inverted vertical center aligned segment" style="min-height: 55vh; padding-bottom: 5vh">
    <div class="ui container" style="padding-top: 5vh">
      <div class="ui secondary inverted menu">
        <div class="right item">
          <a class="ui inverted button" href="<?php echo base_url('index.php/auth') ?>">Log in</a>
        </div>
      </div>
    </div>

    <div class="ui container" style="padding-top: 5vh">
      <div class="ui vertical segment">
          <div class="ui middle aligned stackable grid container">
            <div class="row">
              <div class="eight wide right floated column">
                <img class="ui centered large image" src="<?php echo base_url() ?>/assets/img/logo-cloud-dds.png">
              </div>
              <div class="eight wide column">
                <h2 class="ui inverted header">Layanan IaaS<br />
                <span style="font-weight: 100">(Infrastructure as a Service)</span>
                </h2>
                <h2 class="ui inverted header">Telkom Divisi Digital Service</h2>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="ui vertical segment" style="min-height: 45vh; padding-bottom: 5vh">
    <div class="ui centered page grid" style="padding-top: 5vh">
        <div class="fourteen wide column">
            <div class="ui three column center aligned stackable grid">
                <div class="column">
                    <img class="ui tiny centered image" src="assets/img/cloud-computing.png">
                    <h1 class="ui header">Freedom at VM Level</h1>
                </div>
                <div class="column">
                    <img class="ui tiny centered image" src="<?php echo base_url() ?>/assets/img/list.png">
                    <h1 class="ui header">Easy &amp; On-Demand Activation</h1>
                </div>
                <div class="column">
                        <img class="ui tiny centered image" src="<?php echo base_url() ?>/assets/img/scaleup.png">
                    <h1 class="ui header ng-binding">Scale Resource As You Grow</h1>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="ui vertical stripe flavor segment">
    <div class="ui text container center aligned">
        <h3 class="ui header">Aktivasi Mudah dan Cepat</h3>
        <p>Tersedia beragam pilhan sesuai kebutuhan</p>
    </div>

    <br />
    <br />

    <div class="ui container">
        <div class="ui four column stackable grid">
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui ribbon label red">General</a>
                    <div class="ui list">
                        <div class="item">1 vCPU</div>
                        <div class="item">1GB Memory</div>
                        <div class="item">20GB Storage</div>
                    </div>
					<br>
					  <div class="ui bulleted list">
						  <div class="item">Low &amp; Medium-traffic Websites and Web Applications</div>
						  <div class="item">Development environments</div>
						  <div class="item">Code repositories</div>
						  <div class="item">Micro services</div>
						  <div class="item">Test &amp; Staging environments</div>
						  <div class="item">Small and Mid-size database</div>
					  </div>
					<br>
                </div>
            </div>
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui ribbon label yellow">Compute Optimized</a>
                    <div class="ui list">
                        <div class="item">2 vCPU</div>
                        <div class="item">2GB Memory</div>
                        <div class="item">20GB Storage</div>
                    </div>
					<br>
					  <div class="ui bulleted list">
						  <div class="item">High-traffic Web server</div>
						  <div class="item">Batch processing</div>
						  <div class="item">Distributed Analytics</div>
					  </div>
					<br><br><br><br><br>
                </div>
            </div>
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui ribbon label green">Memory Optimized</a>
                    <div class="ui list">
                        <div class="item">2 vCPU</div>
                        <div class="item">4GB Memory</div>
                        <div class="item">40GB Storage</div>
                    </div>
					<br>
					  <div class="ui bulleted list">
						  <div class="item">High-performance database</div>
						  <div class="item">Data mining &amp; analysis</div>
						  <div class="item">Applications performing real-time processing of unstructured big data</div>
					  </div>
					<br><br><br>
                </div>
            </div>
            <div class="column">
                <div class="ui raised segment">
                    <a class="ui ribbon label blue">Storage Optimized</a>
                                        <div class="ui list">
                        <div class="item">2 vCPU</div>
                        <div class="item">2GB Memory</div>
                        <div class="item">80GB Storage</div>
                    </div>
					<br>
					  <div class="ui bulleted list">
						  <div class="item">Data warehousing</div>
						  <div class="item">Log or data-processing applications</div>
						  <div class="item">Distributed file systems</div>
						  <div class="item">Network file systems</div>
					  </div>
					<br><br><br>
                </div>
            </div>
        </div>
    </div>

</div>

    <div class="ui vertical center aligned segment">
      <div class="ui container">
        <div class="ui small images">
          <img class="ui image logo" src="<?php echo base_url() ?>/assets/img/logo-telkom-indonesia.png">
          <img class="ui image logo" src="<?php echo base_url() ?>/assets/img/dds-logo.png">
          <img class="ui image logo" src="<?php echo base_url() ?>/assets/img/OpenStack-Logo.png">
        </div>
      </div>
    </div>

    <div class="ui inverted vertical footer segment">
      <div class="ui container">
        <div class="ui stackable inverted equal height center aligned stackable grid">
          <div class="seven wide column">
            <h4 class="ui inverted header">PT. Telekomunikasi Indonesia, Tbk</h4>
            <p>Divisi Digital Service<br> Jl. Gegerkalong Hilir No. 47. Bandung 40152.</p>
          </div>
        </div>
      </div>
    </div>
</div>


<?php // echo form_close(); ?>

<?php $this->load->view('footer'); ?>