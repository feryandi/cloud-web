<?php $this->load->view('header'); ?>

<?php echo form_open('dashboard/request',array("class"=>"form-horizontal")); ?>
		
	<div class="ui teal inverted vertical center aligned segment">
		<div class="ui container">
			<div class="ui secondary inverted menu">
				<div class="left item">
					<a class="item" href="<?php echo base_url() ?>">
						<img class="ui small image" src="../../assets/img/logo-cloud-dds.png">
					</a>
				</div>
				<div class="right item">
					<a class="item" href="<?php echo base_url('index.php/dashboard/index') ?>">My Virtual Machine(s)</a>
					<div class="ui top right pointing dropdown" style="margin-left: 1.5em;" tabindex="0">
						<img class="ui avatar image" src="../../assets/img/avatar/user.png">
						<i class="dropdown icon"></i>
						<div class="menu" tabindex="-1">
							<a class="item">
								<img class="ui avatar image" src="../../assets/img/avatar/user.png">
								<span><?php echo $name ?></span>
							</a>
							<div class="divider"></div>
							<a class="item" href="<?php echo base_url('index.php/auth/logout') ?>">Logout</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="ui container">
		<br>
		<?php if(validation_errors() != false) { ?>
		<div class="ui form">
		  <div class="ui negative message">
		    <div class="header">Error on Your Input</div>
		    <div class="ui divider"></div>
		    <ul class="list">
				<?php echo validation_errors(); ?>
		    </ul>
		  </div>
		</div>
		<?php } ?>

		<!-- Choose OS -->
		<div class="ui container">
		
			<!-- User Name -->
			<input type="hidden" id="user_name" name="user_name" value="<?php echo $name ?>">
			
			<!-- Input Image -->
			<input type="hidden" id="prev-os-value" name="prev-os-value" value="default">
			<input type="hidden" id="os-value" name="os-value" value="default">
			<input type="hidden" id="os-name" name="os-name" value="default">

			<!-- Input Flavor -->
			<input type="hidden" id="prev-flavor-value" name="prev-os-value" value="default">
			<input type="hidden" id="flavor-value" name="flavor-value" value="default">

			<br />

			<h2 class="ui dividing header">Choose Operating System</h2>

			<div class="ui padded container">

				<div class="ui four column stackable grid">
					
					<?php foreach($images as $image){ ?>
					
					<div class="column">

						<div id="<?php echo "image-" . $image->id ?>" class="ui center aligned segment" onclick="setOSValue('<?php echo $image->id ?>', '<?php echo $image->name ?>')">

							<div class="ul list">

							<br/>
								<div class="item"><?php echo $image->name ?></div>
							<br/>

							</div>


						</div>

					</div>
					
					<?php }?>
					
				</div>

			</div>
			
		<br/><br/><br/>
		
		<!-- Choose Size -->
		<div class="ui container">
			<h2 class="ui dividing header">Choose VM Size</h2>

			<div class="ui top attached tabular menu">

				<a class="item active" data-tab="default_size">Default Size</a>
				<!--<a class="item" data-tab="#">Custom Size</a>-->

			</div>

			<div class="ui bottom attached active tab segment">

				<div class="ui padded container">

					<div class="ui four column stackable grid">
						
						<?php foreach($flavors as $flavor){ ?>
						
						<div class="column">

							<div id="<?php echo "image-" . $flavor->id ?>" class="ui center aligned segment" onclick="setFlavorValue('<?php echo $flavor->id ?>')">
								<div class="ul list">
									<div class="item"><?php echo $flavor->name ?></div>

									<div class="ui divider"></div>

									<div class="item"><?php echo substr($flavor->id, 0, 1) ?> VCPUs</div>
									<div class="item"><?php echo $flavor->id[2] ?> GB RAM</div>
									<div class="item"><?php echo substr($flavor->id, 4, strlen($flavor->id)-1) ?> GB Storage</div>
									<br />

								</div>

							</div>

						</div>
						
						<?php }?>
						
					</div>

				</div>

			</div>

			<!-- <div class="ui bottom attached tab segment" data-tab="custom_size">

				<div class="eight wide field"></div>

			</div> -->

		</div>

		<!-- Key Pair 
		<br/><br/><br/>
		<div class="ui container">

			<h2 class="ui dividing header">Key Pair</h2>

			<div class="ui form">

				<div class="ui styled fluid accordion">

					<div class="active title">

						<i class="dropdown icon"></i>
						Create new Key Pair

					</div>
				<div class="active content">

						<div class="ui form">

							<div class="eight wide field">

								<label for="keypair_name">1. Enter a name for your key pair (e.g jdoekey):</label>
								<input type="text" name="keypair_name" placeholder="Key pair name">

							</div>

							<div class="eight wide field">

								<label>2. Click to create your key pair:</label>
								<button class="ui teal button">Create & Download your Key Pair</button>

							</div>

						</div>
				</div>
				<div class="title">

					<i class="dropdown icon"></i>
					Choose a Key Pair

				</div>

					<div class="content">

						<div class="ui form">
							<div class="inline fields">
								<div class="field" id="list-keypair">
									
									<?php //foreach($keypairs as $keypair){ ?>
									
									<div class="ui radio checkbox">
										<input type="radio" name="keypair" value="<?php //echo $keypair->name ?>">
										<label><?php //echo $keypair->name ?></label>
									</div>
									
									<?php //}?>

								</div>
							</div>
						</div>
						
					</div>

				</div>

			</div>

		</div>-->

		<br/><br/><br/>
		<!-- Finalize -->
		<div class="ui container">

			<h2 class="ui dividing header">Finalize and Create</h2>

			<div class="ui form">

				<div class="eight wide field">

					<label>Choose a name</label>
					<input type="text" name="hostname" placeholder="VM Name">

				</div>

				<div class="field">

					<label>Usage</label>
					<textarea rows="3" name="usage_purpose"></textarea>

				</div>

				<div class="fields">

					<div class="field">

						<label>Supervisor Name</label>
						<input type="text" name="supervisor" placeholder="Name">

					</div>

					<div class="field">

						<label>Supervisor Phone Number</label>
						<input type="text" name="supervisor_phone_number" placeholder="Phone Number">

					</div>

				</div>

			</div>

		</div>

		<button class="ui fluid teal button">Request VM</button>

		</div>
		<br/><br/><br/>
		
	</div>

	<script>

        $('.ui.dropdown')
            .dropdown();

		$('.tabular.menu .item').tab();

		function setOSValue(x, name){
			document.getElementById("os-value").value = x;
			document.getElementById("os-name").value = name;

			if(document.getElementById("prev-os-value").value == "default")
			{
				document.getElementById("prev-os-value").value = x;
				document.getElementById("image-" + x).setAttribute("style", "background-color: #00b5ad;color: white;");
			}
			else
			{
				document.getElementById("image-" + document.getElementById("prev-os-value").value).setAttribute("style", "background-color: white;color: rgba(0,0,0,.87);");
				document.getElementById("image-" + x).setAttribute("style", "background-color: #00b5ad;color: white;");
				document.getElementById("prev-os-value").value = x;
			}
		}

		function setFlavorValue(y) {
			document.getElementById("flavor-value").value = y;

			if(document.getElementById("prev-flavor-value").value == "default")
			{
				document.getElementById("prev-flavor-value").value = y;
				document.getElementById("image-" + y).setAttribute("style", "background-color: #00b5ad;color: white;");
			}
			else
			{
				document.getElementById("image-" + document.getElementById("prev-flavor-value").value).setAttribute("style", "background-color: white;color: rgba(0,0,0,.87);");
				document.getElementById("image-" + y).setAttribute("style", "background-color: #00b5ad;color: white;");
				document.getElementById("prev-flavor-value").value = y;
			}
		}

	</script>



<?php echo form_close(); ?>
<?php $this->load->view('footer'); ?>