<?php $this->load->view('header'); ?>
	
	<div class="ui teal inverted vertical center aligned segment">
		<div class="ui container">
			<div class="ui secondary inverted menu">
				<div class="left item">
					<a class="item" href="<?php echo base_url() ?>">
						<img class="ui small image" src="../../assets/img/logo-cloud-dds.png">
					</a>
				</div>
				<div class="right item">
					<a class="item" href="<?php echo base_url('index.php/dashboard/index') ?>">MANAGE VMs</a>
					<a class="item" href="<?php echo base_url('index.php/dashboard/requestedvm') ?>">REQUESTED VMs</a>
					<a class="item" href="<?php echo base_url('index.php/dashboard/request') ?>">CREATE VM</a>
					<a class="item" href="/manage/help">HELP</a>
					<div class="ui top right pointing dropdown" style="margin-left: 1.5em;" tabindex="0">
						<img class="ui avatar image" src="../../assets/img/avatar/user.png">
						<i class="dropdown icon"></i>
						<div class="menu" tabindex="-1">
							<a class="item">
								<img class="ui avatar image" src="../../assets/img/avatar/user.png">
								<span><?php echo $name ?></span>
							</a>
							<div class="divider"></div>
							<a class="item" href="/manage/settings">Settings</a>
							<a class="item" href="<?php echo base_url('index.php/auth/logout') ?>">Logout</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<div class="ui vertical stripe computes container">
    <div class="ui stackable grid container">
        <div class="column">
            <h3 class="ui header">Manage Instance</h3>
            <a href="/manage/computes">Back.</a>
        </div>
    </div>

    <div class="ui stackable grid container">
        <div class="four wide column">
            <table class="ui teal table">
                <tbody>
                    <tr>
                        <td>Hostname</td>
                        <td>{{server['server']['name']}}</td>
                    </tr>
                    <tr>
                        <td>IP Address</td>
                        <td>{{server['server']['addresses']['external_network'][0]['addr']}}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{{ serverins.request_at }}</td>
                    </tr>
                    <tr>
                        <td>Approval Date</td>
                        <td>{{ serverins.build_on }}</td>
                    </tr>
                    <tr>
                        <td>PIC Name</td>
                        <td>{{ serverins.pic_name }}</td>
                    </tr>
                    <tr>
                        <td>PIC Telp</td>
                        <td>{{ serverins.pic_telp }}</td>
                    </tr>
                    <tr>
                        <td>Key Name</td>
                        <td>{{server['server']['key_name']}}</td>
                    </tr>
                </tbody>
            </table>
            </br>
            <table class="ui teal table">
                <tbody>
                    {% if console %}
                        <tr>
                            <td>Open Console</td>
                            <td><a class="ui teal button" href="{{ console['console']['url'] }}" target="_blank">Console</a></td>
                        </tr>
                    {% else %}
                        <tr>
                            <td colspan="2">Console Unavailable</td>
                        </tr>
                    {% endif %}
                    <tr>
                        <td>Scale Up Instance</td>
                        <td><button class="ui blue button scale">Scale Up</button></td>
                    </tr>
                    <tr>
                        <td>Delete Instance</td>
                        <td><button class="ui red button deletevm">Delete</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="twelve wide column">
            <table class="ui teal table">
                <thead>
                    <tr>
                        <th>Operating System : {{ serverins.image_name }}</th>
                        {% if uptime %}
                            <th>Uptime : {{ uptime }}</th>
                        {% else %}
                            <th>Uptime : Undefined</th>
                        {% endif %}
                    </tr>
                </thead>
            </table>
            </br>
            <table class="ui teal fixed table">
                <thead class="ui center aligned">
                    <tr>
                        <th>CPU Uptime</th>
                        <th>RAM</th>
                        <th>Storage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="ui center aligned container">
                                <div class="ui statistic">
                                    <div class="value">
                                        {% if diagnostics %}
                                            {{ diagnostics["cpu0_time"]/1000000000 }}
                                        {% else %}
                                            Undefined
                                        {% endif %}
                                    </div>
                                    <div class="label">
                                        Second
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <canvas id="ram_chart"></canvas>
                        </td>
                        <td>
                            <canvas id="storage_chart"></canvas>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="ui scale modal">
    <i class="close icon"></i>
    <div class="header">
        Request Scale Up VM
    </div>
    <div class="content">
        <div class="ui form">
            <div class="field" id="slider">
                <label for="custom_vcpu">CPU : </label>
                <input type="text" id="custom_vcpu" readonly style="border:0; color:#00b5ad; font-weight:bold;">
                <div id="slider-range-max-vcpu"></div>

                <div class="ui grid" style="margin-top: .5em;">
                    <div class="left floated wide column">1vCPU</div>
                    <div class="right floated wide column">2vCPU</div>
                </div>
            </div>
            <div class="field" id="slider">
                <label for="custom_memory">Memory : </label>
                <input type="text" id="custom_memory" readonly style="border:0; color:#00b5ad; font-weight:bold;">
                <div id="slider-range-max-memory"></div>
                <div class="ui grid" style="margin-top: .5em;">
                    <div class="left floated six wide column">1 GB</div>
                    <div class="left floated five wide column" style="margin-left: 2em;">2 GB</div>
                    <div class="right floated wide column">4 GB</div>
                </div>
            </div>
            <div class="field" id="slider">
                <label for="custom_storage">Storage : </label>
                <input type="text" id="custom_storage" readonly style="border:0; color:#00b5ad; font-weight:bold;">
                <div id="slider-range-storage"></div>
                <div class="ui grid" style="margin-top: .5em;">
                    <div class="left floated six wide column">20GB</div>
                    <div class="left floated five wide column" style="margin-left: 2em;">40GB</div>
                    <div class="right floated wide column">80GB</div>
                </div>
            </div>
            </br>
            <button class="ui fluid teal submit button scale" type="submit" name="btn" value="scale">Request</button>
        </div>
    </div>
</div>

<div class="ui deletevm modal">
    <i class="close icon"></i>
    <div class="header">
        Delete VM
    </div>
    <div class="content">
        <p>VM akan di non-aktifkan dan tidak dapat digunakan lagi.</p>
    </div>
    <div class="actions">
        <form id="form-delete" class="ui form" method="POST">
            <input id="action" name="action" hidden value="delete">
            <button class="ui teal cancel button">Batal</button>
            <button class="ui red ok button submit" onclick="submit('form-delete')">Ya</button>
        </form>
    </div>
</div>

<script>

    function submit(form_id){
        alert(form_id)
    };

    $('.menu.computes .item')
        .tab()
        ;

    {% if diagnostics %}
        var ram_pie = document.getElementById("ram_chart").getContext("2d");
        var ram_chart = new Chart(ram_pie, {
            type: 'pie',
            data: {
                labels: [
                    "RAM Used Space",
                    "RAM Free Space"
                ],
                datasets: [
                    {
                        label: 'MB',
                        data: [{{ (diagnostics["memory-actual"] - diagnostics["memory-unused"])/1024 }},{{diagnostics["memory-unused"]/1024}}],
                        backgroundColor: [
                            "#00B5AD",
                            "#A0A0A0"
                        ]
                    }
                ]
            },
            options: {
                animation: {
                    animateScale: true
                },
                responsive: true
            }
        });
    {% else %}
        var ram_pie = document.getElementById("ram_chart").getContext("2d");
        var ram_chart = new Chart(ram_pie, {
            type: 'pie',
            data: {
                labels: [
                    "RAM Used Space",
                    "RAM Free Space"
                ],
                datasets: [
                    {
                        label: 'MB',
                        data: [1,1],
                        backgroundColor: [
                            "#00B5AD",
                            "#A0A0A0"
                        ]
                    }
                ]
            },
            options: {
                animation: {
                    animateScale: true
                },
                responsive: true
            }
        });
    {% endif %}


    var storage_pie = document.getElementById("storage_chart").getContext("2d");
    var storage_chart = new Chart(storage_pie, {
        type: 'pie',
        data: {
            labels: [
                "storage Used Space",
                "storage Free Space"
            ],
            datasets: [
                {
                    label: 'GB',
                    data: [20, 12],
                    backgroundColor: [
                        "#00B5AD",
                        "#A0A0A0"
                    ]
                }
            ]
        },
        options: {
            animation: {
                animateScale: true
            },
            responsive: true
        }
    });

    $('.ui.scale.modal')
        .modal('attach events', '.ui.button.scale', 'show')
        .modal('setting', 'transition', 'fade up');

    $(function () {
        var objCpu = [1, 2];
        $("#slider-range-max-vcpu").slider({
            range: "max",
            min: 1,
            max: objCpu.length,
            value: 1,
            slide: function (event, ui) {
                $("#custom_vcpu").val(objCpu[ui.value - 1]);
                $("#custom_vcpu_form").val(objCpu[ui.value - 1]);
            }
        });
        $("#custom_vcpu").val(objCpu[$("#slider-range-max-vcpu").slider("value") - 1]);
    });

    $(function () {
        var objMemory = [1, 2, 4];
        $("#slider-range-max-memory").slider({
            range: "max",
            min: 1,
            max: objMemory.length,
            value: 1,
            slide: function (event, ui) {
                $("#custom_memory").val(objMemory[ui.value - 1]);
                $("#custom_memory_form").val(objMemory[ui.value - 1]);
            }
        });
        $("#custom_memory").val(objMemory[$("#slider-range-max-memory").slider("value") - 1]);
    });

    $(function () {
        var objStorage = [20, 40, 80];
        $("#slider-range-storage").slider({
            range: "max",
            min: 1,
            max: objStorage.length,
            value: 1,
            slide: function (event, ui) {
                $("#custom_storage").val(objStorage[ui.value - 1]);
                $("#custom_storage_form").val(objStorage[ui.value - 1]);
            }
        });
        $("#custom_storage").val(objStorage[$("#slider-range-storage").slider("value") - 1]);
    });

    $('.ui.deletevm.modal')
        .modal('attach events', '.ui.button.deletevm', 'show')
        .modal('setting', 'transition', 'fade up');

</script>

{% endblock %}
