<?php $this->load->view('header'); ?>
<?php // echo validation_errors(); ?>

<?php // echo form_open('dashboard/create',array("class"=>"form-horizontal")); ?>

        <div class="ui large top fixed hidden menu">
          <div class="ui container">
            <a class="active item">Home</a>
            <a class="item">Work</a>
            <a class="item">Company</a>
            <a class="item">Careers</a>
            <div class="right menu">
              <div class="item">
                <a class="ui button">Log in</a>
              </div>
            </div>
          </div>
        </div>

            <div class="ui teal inverted vertical center aligned segment">

                <div class="ui container">
                    <div class="ui secondary inverted menu">
                        <div class="left item">
                            <a class="item" href="/manage">
                                <img class="ui small image" src="../../assets/img/logo-cloud-dds.png">
                            </a>
                        </div>
                        <div class="right item">
                            <a class="item" href="<?php echo base_url('index.php/dashboard/request') ?>">CREATE VM</a>
                            <a class="item" href="/manage/help">HELP</a>
                            <div class="ui top right pointing dropdown" style="margin-left: 1.5em;" tabindex="0">
                                <img class="ui avatar image" src="/static/img/avatar/user.png">
                                <i class="dropdown icon"></i>
                                <div class="menu" tabindex="-1">
                                    <a class="item">
                                        <img class="ui avatar image" src="/static/img/avatar/user.png">
                                        <span><?php echo $name ?></span>
                                    </a>
                                    <div class="divider"></div>
                                    <a class="item" href="/manage/settings">Settings</a>
                                    <a class="item" href="<?php echo base_url('auth/logout') ?>">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <div class="ui padded container segment">
	
		<?php foreach($vms as $vm){ ?>
			
        <div class="ui two column stackable grid">
			<div class="column">
				<div class="ui segment">
					<div class="ui two column stackable grid">
						<div class="column">
							Image : <?php echo $vm->image ?><br/>
							Flavor : <?php echo $vm->flavor ?><br/>
							Keypair : <?php echo $vm->keypair ?><br/>
							Hostname : <?php echo $vm->hostname ?><br/>
							Supervisor : <?php echo $vm->supervisor ?><br/>
							Supervisor Phone Number : <?php echo $vm->supervisor_phone_number ?><br/>
						</div>
						<div class="column">
							Date Request : <?php echo $vm->date_request ?><br/>
							Usage Purpose : <?php echo $vm->usage_purpose ?><br/>
						</div>
					</div>
				</div>
			</div>
			<div class="column">
				<button class="ui teal button">Restart</button>
				<button class="ui teal button">Delete</button>
			</div>
        </div>
		
		<?php } ?>
		
    </div>

    <script>
        $('.ui.dropdown')
            .dropdown()
        ;
    </script>
	
<?php // echo form_close(); ?>

<?php $this->load->view('footer'); ?>