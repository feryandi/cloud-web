<?php $this->load->view('header'); ?>
<?php // echo validation_errors(); ?>

<?php // echo form_open('dashboard/create',array("class"=>"form-horizontal")); ?>
    <div class="ui teal inverted vertical center aligned segment">
		<div class="ui container">
			<div class="ui secondary inverted menu">
				<div class="left item">
					<a class="item" href="<?php echo base_url() ?>">
						<img class="ui small image" src="../../assets/img/logo-cloud-dds.png">
					</a>
				</div>
				<div class="right item">
					<a class="item" href="<?php echo base_url('index.php/dashboard/index') ?>">MANAGE VMs</a>
					<a class="item" href="<?php echo base_url('index.php/dashboard/requestedvm') ?>">REQUESTED VMs</a>
					<a class="item" href="<?php echo base_url('index.php/dashboard/request') ?>">CREATE VM</a>
					<a class="item" href="/manage/help">HELP</a>
					<div class="ui top right pointing dropdown" style="margin-left: 1.5em;" tabindex="0">
						<img class="ui avatar image" src="../../assets/img/avatar/user.png">
						<i class="dropdown icon"></i>
						<div class="menu" tabindex="-1">
							<a class="item">
								<img class="ui avatar image" src="../../assets/img/avatar/user.png">
								<span><?php echo $name ?></span>
							</a>
							<div class="divider"></div>
							<a class="item" href="/manage/settings">Settings</a>
							<a class="item" href="<?php echo base_url('index.php/auth/logout') ?>">Logout</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


		<div class="ui stackable grid container">
	        <div class="row">
	            <div class="ui vertical stripe center aligned container">
	                <h2>List of Requested VM</h2>
	                <table class="ui teal table center aligned">
	                    <thead>
	                        <tr>
	                            <th>No.</th>
	                            <th>Operating System</th>
	                            <th>Size</th>
	                            <th>Supervisor</th>
	                            <th>Usage Purpose</th>
	                            <th>Date Request</th>
	                            <th>Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        
	                        <?php $i = 1; foreach($vms as $vm){ ?>
	                        <tr>
	                            <td>
									<?php echo $i; $i++;?>
	                            </td>
	                            <td>
									<?php echo $vm->image; ?>
	                            </td>
	                            <td>
									<?php 
										echo substr($vm->flavor, 0, 1) . " VCPUs<br/>";
										echo $vm->flavor[2] . " GB RAM<br/>";
										echo substr($vm->flavor, 4, strlen($vm->flavor)-1) . " GB Disk";
									?>
	                            </td>
	                            <td>
									<?php echo $vm->supervisor; ?>
								</td>
	                            
	                            <td>				
									<?php echo $vm->usage_purpose; ?>
								</td>
								
								<td>
									<?php echo $vm->date_request; ?>
								</td>
	                            
	                            <td>
									<a href="<?php echo site_url('dashboard/cancelvm/' . $vm->identifier_vm); ?>"><button class="ui yellow button">Cancel Request</button></a>
	                            </td>
	                        </tr>
	                        <?php }?>
	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>

		<script>
			$('.ui.dropdown')
				.dropdown()
			;
		</script>
		
<?php // echo form_close(); ?>

<?php $this->load->view('footer'); ?>