<?php $this->load->view('header'); ?>
<?php // echo validation_errors(); ?>

<?php // echo form_open('dashboard/create',array("class"=>"form-horizontal")); ?>
    <div class="ui teal inverted vertical center aligned segment">
		<div class="ui container">
			<div class="ui secondary inverted menu">
				<div class="left item">
					<a class="item" href="<?php echo base_url() ?>">
						<img class="ui small image" src="../../assets/img/logo-cloud-dds.png">
					</a>
				</div>
				<div class="right item">
					<a class="item active" href="<?php echo base_url('index.php/dashboard/index') ?>">My Virtual Machine(s)</a>
					<div class="ui top right pointing dropdown" style="margin-left: 1.5em;" tabindex="0">
						<img class="ui avatar image" src="../../assets/img/avatar/user.png">
						<i class="dropdown icon"></i>
						<div class="menu" tabindex="-1">
							<a class="item">
								<img class="ui avatar image" src="../../assets/img/avatar/user.png">
								<span><?php echo $name ?></span>
							</a>
							<div class="divider"></div>
							<a class="item" href="<?php echo base_url('index.php/auth/logout') ?>">Logout</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php if($sum == 0){ ?>
	
		<div class="ui stackable grid container">
			<div class="row">
				<div class="ui vertical center aligned container" style="padding-top: 10vh">
					<div class="ui segment">
						<br>
						<br>

						<h2>You don't have any Virtual Machine.</h2>

						<h4>Let’s create one.</h4>

						<a href="<?php echo base_url('index.php/dashboard/request') ?>">
							<button class="ui teal button">
								<i class="plus icon"></i> Create Virtual Machine
							</button>
						</a>

						<br>
						<br>
						<br>
					</div>
				</div>
			</div>
		</div>

	<?php }else{ ?>
		
		<div class="ui stackable grid container">
	        <div class="row">
	            <div class="ui vertical stripe center aligned container">
	                <h2>My Virtual Machine(s)</h2>
	                <table class="ui teal table center aligned">
						<a href="<?php echo base_url('index.php/dashboard/request') ?>">
							<button class="ui teal button right floated">
								<i class="plus icon"></i>
								Request New Virtual Machine
							</button>
						</a>
						<br/>
						<br/>
	                    <thead>
	                        <tr>
	                            <th>No.</th>
	                            <th>Name</th>
	                            <th>Operating System</th>
	                            <th>Size</th>
	                            <th>IP Address</th>
	                            <th>Status</th>
	                            <th>Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        
	                        <?php 
								$i = 1; 
								//print_r($vms);
								//exit;
								foreach($vms as $vm){ 
									if(($vm->status == 1) or ($vm->status == 2) or ($vm->status == 3)){
							?>
	                        <tr>
								<td>
									<?php echo $i; $i++;?>
								</td>
	                            <td>
									<?php 
										if($vm->status != 4){
											echo $vm->hostname;
										}
									?>
								</td>
	                            <td>
									<?php 
										if($vm->status != 4){
											echo $vm->image;
										}
									?>
	                            </td>
	                            <td>
									<?php 
										if($vm->status != 4){
											echo substr($vm->flavor, 0, 1) . " VCPUs<br/>";
											echo $vm->flavor[2] . " GB RAM<br/>";
											echo substr($vm->flavor, 4, strlen($vm->flavor)-1) . " GB Disk";
										}
									?>
	                            </td>
	                            <td>
									<?php
										if($vm->ip_vm == "default"){
											echo 'not available';
										}else{
											echo $vm->ip_vm;
										}
									?>
	                            </td>
	                            <td>
									<?php
										if($vm->status == 1){
											echo "Waiting for approval";
										}elseif($vm->status == 2){
											echo "Running";
										}elseif($vm->status == 3){
											echo "Declined by administrator";
										}
									?>					
								</td>
	                            
	                            <td>
									<?php 
										if($vm->status == 2){
									?>
									
										<?php $console = base_url('index.php/dashboard/getConsolevm/' . $vm->id_vm) ?>
										<a href="<?php echo $console ?>" target="_blank"><button class="ui black button console" target="_blank">Console</button></a>
										
										<a href="<?php echo base_url('index.php/dashboard/rebootvm/' . $vm->identifier_vm) ?>" onclick="return confirm('Are you sure you want to reboot VM?');"><button class="ui yellow button">Reboot</button></a>
										
										<a href="<?php echo base_url('index.php/dashboard/deletevm/' . $vm->identifier_vm) ?>" onclick="return confirm('Are you sure you want to delete VM?');"><button class="ui red button">Delete</button></a>
									
									<?php 
										}elseif($vm->status == 1){
									?>
									
										<a href="<?php echo site_url('dashboard/cancelvm/' . $vm->identifier_vm); ?>"><button class="ui yellow button">Cancel Request</button></a>
									
									<?php 
										}else{
									?>
									
										no action available
									
									<?php 
										}
									?>
									
	                                <!--<div class="ui popup top left transition hidden">
	                                    <div class="ui one column divided center aligned grid">
	                                        <div class="column">
												Are you sure want to delete this vm
	                                            <button class="ui button">Yes</button>
	                                        </div>
	                                    </div>
	                                </div>-->
									
	                            </td>
	                        </tr>
	                        <?php
									}
								}
							?>
	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
		
	<?php }?>
	
<script>
	$('.ui.dropdown')
		.dropdown()
	;
</script>
		
<?php // echo form_close(); ?>

<?php $this->load->view('footer'); ?>