<!DOCTYPE Html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="description" content="Layanan cloud DDS Telkom" />
    <title>Login Cloud - DDS Telkom</title>
    <link rel="icon" href="../assets/favicon.png">
    <link rel="stylesheet" type="text/css" href="../assets/css/semantic.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css" />
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/semantic.min.js"></script>
</head>

<body class="login-signup">

    <a href="#"><img class="ui medium image logo" id="logo" src="../assets/img/logo-cloud-dds.png"></a>

    <div class="ui middle aligned center aligned grid">
        <div class="login-signup column">
		
            <div class="ui negative message"><?php echo $message;?></div>
			
            <?php echo form_open("auth/login");?>
			
                <div class="ui segment">
                    <h1 class="ui text header logo">
                        Log In
                    </h1>
                    <div class="two fields">
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="mail icon"></i>
                                <input type="text" value="<?php echo $this->input->post('identity'); ?>" name="identity" placeholder="E-mail address">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="at icon"></i>
                                <input type="text" name="text" readonly value="telkom.co.id">
                                <input type="hidden" name="email_domain" value="@telkom.co.id">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="field">
						<div class="ui checkbox">
							<input type="checkbox" tabindex="0" class="hidden">
							<label>Remember</label>
						</div>
                    </div>
                    <button class="ui fluid large teal submit button" type="submit">Login</button>
                    </br>
                </div>

                <div class="ui error message"></div>

            </form>

            <div class="ui message">
                <p class="ui header">Tidak punya akun ? <a href="/registration">Sign Up</a></p>
            </div>
        </div>
    </div>
</body>

</html>
