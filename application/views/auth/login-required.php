<!DOCTYPE Html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="description" content="Layanan cloud DDS Telkom" />
    <title>Login Cloud - DDS Telkom</title>
    <link rel="icon" href="../assets/favicon.png">
    <link rel="stylesheet" type="text/css" href="../assets/css/semantic.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css" />
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/semantic.min.js"></script>
</head>

<body class="error-page">
    <a href="{{url_for('index')}}">
        <img class="ui large image logo" id="logo" src="../assets/img/logo-cloud-dds.png"></a>
    <div class="ui middle aligned center aligned grid">
        <div class="error column">
            <div class="ui segment">
                <div class="ui text header logo">
                    <h1>Login Required</h1>
                    <p>back to <a href="/login">login page</a>.</p>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
