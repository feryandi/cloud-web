<?php $this->load->view('header'); ?>
<?php // echo validation_errors(); ?>

<?php // echo form_open('dashboard/create',array("class"=>"form-horizontal")); ?>
    <div class="ui teal inverted vertical center aligned segment">
		<div class="ui container">
			<div class="ui secondary inverted menu">
				<div class="left item">
					<a class="item" href="<?php echo base_url() ?>">
						<img class="ui small image" src="../../assets/img/logo-cloud-dds.png">
					</a>
				</div>
				<div class="right item">
					<a class="item active" href="<?php echo base_url('index.php/admin/index') ?>">MANAGE VMs</a>
					<a class="item" href="<?php echo base_url('index.php/admin/requestedvm') ?>">REQUESTED VMs</a>
					<a class="item" href="<?php echo base_url('index.php/admin/rejectedvm') ?>">REJECTED VMs</a>
					<div class="ui top right pointing dropdown" style="margin-left: 1.5em;" tabindex="0">
						<img class="ui avatar image" src="../../assets/img/avatar/user.png">
						<i class="dropdown icon"></i>
						<div class="menu" tabindex="-1">
							<a class="item">
								<img class="ui avatar image" src="../../assets/img/avatar/user.png">
								<span><?php echo $name ?></span>
							</a>
							<div class="divider"></div>
							<a class="item" href="<?php echo base_url('index.php/auth/logout') ?>">Logout</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


		<div class="ui stackable grid container">
	        <div class="row">
	            <div class="ui vertical stripe center aligned container">
	                <h2>List of All VMs</h2>
	                <table class="ui teal table center aligned">
	                    <thead>
	                        <tr>
	                            <th>No.</th>
	                            <th>User</th>
	                            <th>Operating System</th>
	                            <th>Size</th>
	                            <th>Date Request</th>
	                            <th>Status</th>
	                            <th>Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        
	                        <?php $i = 1; foreach($vms as $vm){ ?>
	                        <tr>
	                            <td>
									<?php echo $i; $i++; ?>
	                            </td>
	                            <td>
									<?php //echo substr($vm->identifier_vm, 0, 8) . " ... " ; ?>
									<?php echo $vm->user_name ?>
								</td>
	                            <td>
									<?php echo $vm->image; ?>
	                            </td>
	                            <td>
									<?php 
										echo substr($vm->flavor, 0, 1) . " VCPUs<br/>";
										echo $vm->flavor[2] . " GB RAM<br/>";
										echo substr($vm->flavor, 4, strlen($vm->flavor)-1) . " GB Disk";
									?>
	                            </td>
	                            <td>
									<?php echo $vm->date_request; ?>
								</td>
	                            
	                            <td>
									<?php
										if($vm->status == 1){
											echo "Waiting for approval";
										}elseif($vm->status == 2){
											echo "Running";
										}elseif($vm->status == 3){
											echo "Declined";
										}elseif($vm->status == 4){
											echo "Deleted";
										}elseif($vm->status == 5){
											echo "Canceled";
										}
									?>					
								</td>
	                            
	                            <td>
									<?php 
										if($vm->status == 2){
									?>
									
										<a href="#"><button class="ui green button">Resize</button></a>
										
										<a href="<?php echo base_url('index.php/admin/rebootvm/' . $vm->identifier_vm) ?>"> <button class="ui yellow button">Reboot</button> </a>
										
										<a href="<?php echo base_url('index.php/admin/deletevm/' . $vm->identifier_vm) ?>"><button class="ui red button">Delete</button><a/>
									<?php 
										}elseif($vm->status == 1){
									?>
										<a href="<?php echo site_url('admin/acceptrequest/' . $vm->identifier_vm); ?>"><button class="ui green button">Accept Request</button></a>
										<a href="<?php echo site_url('admin/declinerequest/' . $vm->identifier_vm); ?>"><button class="ui red button">Decline Request</button></a>
									
									<?php 
										}else{
									?>
										no action available
									<?php 
										}
									?>
									
	                                <!--<div class="ui popup top left transition hidden">
	                                    <div class="ui one column divided center aligned grid">
	                                        <div class="column">
												Are you sure want to delete this vm
	                                            <button class="ui button">Yes</button>
	                                        </div>
	                                    </div>
	                                </div>-->
									
	                            </td>
	                        </tr>
	                        <?php }?>
	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>

		<script>
			$('.ui.dropdown')
				.dropdown()
			;
		</script>
		
<?php // echo form_close(); ?>

<?php $this->load->view('footer'); ?>