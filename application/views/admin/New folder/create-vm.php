<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="description" content="Layanan cloud DDS Telkom" />
		<title>Login Cloud - DDS Telkom</title>
		<link rel="icon" href="../assets/favicon.png">
		<link rel="stylesheet" type="text/css" href="../assets/css/semantic.min.css" />
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css" />
		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/semantic.min.js"></script>
	</head>

<body>
	<div class="ui vertical stripe container">
		<div class="ui stackable grid container">
			<div class="row">
				<h3 class="ui header">Create Virtual Machine</h3>
				<div class="ui container">
					</br>
					<h2 class="ui dividing header">Choose Operating System</h2>
						<div class="ui padded container">
							<div class="ui four column grid">
									<div class="column">
										<div class="ui center aligned segment">
											<div class="ul list">
												<div class="item">nama_osystem</div>
											</div>
											</br>
											<button class="ui fluid teal button">Choose</button>
										</div>
									</div>
									<div class="column">
										<div class="ui center aligned segment">
											<div class="ul list">
												<div class="item">nama_osystem</div>
											</div>
											</br>
											<button class="ui fluid teal button">Choose</button>
										</div>
									</div>
									<div class="column">
										<div class="ui center aligned segment">
											<div class="ul list">
												<div class="item">nama_osystem</div>
											</div>
											</br>
											<button class="ui fluid teal button">Choose</button>
										</div>
									</div>
							</div>
						</div>
				</div>
			</div>

			<div class="row">
				<div class="ui container">
					<h2 class="ui dividing header">Choose VM size</h2>
					<div class="ui top attached tabular menu">
						<a class="item active" data-tab="flavor">Default</a>
						<a class="item" data-tab="custom">Custom</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="flavor">
						<div class="ui padded container">
							<div class="ui four column grid">
									<div class="column">
										<div class="ui center aligned segment">
											<div class="ul list">
												<div class="item">nama_flavor</div>

												<div class="ui divider"></div>

												<div class="item">jumlah_vCPU</div>
												<div>jumlah_RAM</div>
												<div>jumlah_Storage</div>
											</div>
											</br>
											<button class="ui fluid teal button">Choose</button>
										</div>
									</div>
									<div class="column">
										<div class="ui center aligned segment">
											<div class="ul list">
												<div class="item">nama_flavor</div>

												<div class="ui divider"></div>

												<div class="item">jumlah_vCPU</div>
												<div>jumlah_RAM</div>
												<div>jumlah_Storage</div>
											</div>
											</br>
											<button class="ui fluid teal button">Choose</button>
										</div>
									</div>
									<div class="column">
										<div class="ui center aligned segment">
											<div class="ul list">
												<div class="item">nama_flavor</div>

												<div class="ui divider"></div>

												<div class="item">jumlah_vCPU</div>
												<div>jumlah_RAM</div>
												<div>jumlah_Storage</div>
											</div>
											</br>
											<button class="ui fluid teal button">Choose</button>
										</div>
									</div>
							</div>
						</div>
					</div>

					<div class="ui bottom attached tab segment" data-tab="custom">
							<div class="eight wide field">
								<div class="ui range" id="my-range"></div>
							</div>
					</div>
					
				</div>

			<div class="row">
				<div class="ui container">
					<h2 class="ui dividing header">Key Pair</h2>
					<div class="ui form">
						<div class="ui styled fluid accordion">
						  <div class="active title">
							<i class="dropdown icon"></i>
							Choose Key Pair
						  </div>
						  <div class="active content">
							<div class="ui horizontal list">
								<div class="item">
									<div class="ui radio checkbox">
									  <input name="size" value="small" type="radio">
									  <label>keypair_1</label>
									</div>
								</div>
								<div class="item">
									<div class="ui radio checkbox">
									  <input name="size" value="small" type="radio">
									  <label>keypair_2</label>
									</div>
								</div>
								<div class="item">
									<div class="ui radio checkbox">
									  <input name="size" value="small" type="radio">
									  <label>keypair_3</label>
									</div>
								</div>
							</div>
						  </div>
						  <div class="title">
							<i class="dropdown icon"></i>
							Create a new Key Pair
						  </div>
						  <div class="content">
							<div class="ui form">
								<div class="eight wide field">
									<label for="first-name">1. Enter a name for your key pair (e.g. jdoekey):</label>
									<input type="text" name="first-name" placeholder="Key pair name">
								</div>
								<div class="eight wide field">
									<label for="last-name">2. Click to create your key pair:</label>
									<button class="ui teal button">Create & Download your Key Pair</button>
								</div>
							</div>
						  </div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="ui container">
					<h2 class="ui dividing header">Finalize & create</h2>
					<div class="ui form">
						<div class="eight wide field">
							<label>Choose a name</label>
							<input type="text" name="name" placeholder="Instance Name">
						</div>
						<div class="field">
							<label>Usage</label>
							<textarea rows="3"></textarea>
						</div>
						<div class="fields">
							<div class="field">
								<label>Supervisor Name</label>
								<input type="text" name="text" placeholder="Name">
							</div>
							<div class="field">
								<label>Supervisor Phone Number</label>
								<input type="text" name="text" placeholder="Phone Number"">
							</div>
						</div>
					</div>
				</div>
				<br />
				<button id="submit_button" class="ui fluid large teal button">Request</button>
		</div>
	</div>

	<script>

		$('.ui.accordion')
		  .accordion()
		;

		$('.tabular.menu .item').tab();

		$(document).ready(function() {
		$('#my-range').range({
			min: 0,
			max: 1,
			start: 0,
		});
	});

	</script>

	</body>

</html>