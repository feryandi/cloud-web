<?php echo validation_errors(); ?>

<?php echo form_open('dashboard/request'); ?>

	<div>Image : <input type="text" name="image" value="<?php echo $this->input->post('image'); ?>" /></div>
	<div>Flavor : <input type="text" name="flavor" value="<?php echo $this->input->post('flavor'); ?>" /></div>
	<div>Keypair : <input type="text" name="keypair" value="<?php echo $this->input->post('keypair'); ?>" /></div>
	<div>Hostname : <input type="text" name="hostname" value="<?php echo $this->input->post('hostname'); ?>" /></div>
	<div>Usage Purpose : <textarea name="usage_purpose"><?php echo $this->input->post('usage_purpose'); ?></textarea></div>
	<div>Supervisor : <input type="text" name="supervisor" value="<?php echo $this->input->post('supervisor'); ?>" /></div>
	<div>Supervisor Phone Number : <input type="text" name="supervisor_phone_number" value="<?php echo $this->input->post('supervisor_phone_number'); ?>" /></div>
	
	<button type="submit">Save</button>

<?php echo form_close(); ?>