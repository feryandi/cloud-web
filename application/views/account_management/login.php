<!DOCTYPE Html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="description" content="Layanan cloud DDS Telkom" />
    
	<title>Login Cloud - DDS Telkom</title>
	
    <link rel="icon" href="<?php echo base_url('assets/favicon.png')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/semantic.min.css')?>" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css') ?>" />
    <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/semantic.min.js')?>"></script>
	
    <style type="text/css">
        body {
          background-color: #00b5ad;
        }
        body > .grid {
          height: 100%;
        }
        .image {
          margin-top: -100px;
        }
        .column {
          max-width: 450px;
        }
    </style>
</head>

<body class="login-signup">

    <div class="ui middle aligned center aligned grid">


  <div class="column">

      <img src="../assets/img/logo-cloud-dds.png" class="ui medium image header centered">


      <!--<div class="ui error message"></div>-->
	  
	<?php echo form_open('auth', array('class' => 'ui large form'));?>


      <div class="ui stacked segment">


        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="NIK">
          </div>
        </div>


        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Password">
          </div>
        </div>


        <button class="ui fluid large teal submit button" type="submit">Login</button>


      </div>

	<?php echo form_close(); ?>


  </div>

  
</div>
</body>

</html>
