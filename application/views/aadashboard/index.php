<table border="1" width="100%">
    <tr>
		<th>ID</th>
		<th>Id User</th>
		<th>Image</th>
		<th>Flavor</th>
		<th>Keypair</th>
		<th>Hostname</th>
		<th>Usage Purpose</th>
		<th>Supervisor</th>
		<th>Supervisor Phone Number</th>
		<th>Date Request</th>
		<th>Status</th>
		<th>Date Approval</th>
		<th>Id Admin</th>
		<th>Actions</th>
    </tr>
	<?php foreach($list_virtual_machines as $l){ ?>
    <tr>
		<td><?php echo $l['id']; ?></td>
		<td><?php echo $l['id_user']; ?></td>
		<td><?php echo $l['image']; ?></td>
		<td><?php echo $l['flavor']; ?></td>
		<td><?php echo $l['keypair']; ?></td>
		<td><?php echo $l['hostname']; ?></td>
		<td><?php echo $l['usage_purpose']; ?></td>
		<td><?php echo $l['supervisor']; ?></td>
		<td><?php echo $l['supervisor_phone_number']; ?></td>
		<td><?php echo $l['date_request']; ?></td>
		<td><?php echo $l['status']; ?></td>
		<td><?php echo $l['date_approval']; ?></td>
		<td><?php echo $l['id_admin']; ?></td>
		<td>
            <a href="<?php //echo site_url('list_virtual_machine/edit/'.$l['id']); ?>">Edit</a> | 
            <a href="<?php //echo site_url('list_virtual_machine/remove/'.$l['id']); ?>">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>